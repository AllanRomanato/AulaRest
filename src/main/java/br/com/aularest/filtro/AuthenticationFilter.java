package br.com.aularest.filtro;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.json.JSONObject;

import br.com.aularest.annotations.Secured;
import br.com.aularest.mock.Mock;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter{

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String parametro = requestContext.getHeaderString("token");
		JSONObject obj = new JSONObject();
		if(parametro==null) {
			obj.put("success", false);
			obj.put("message", "Token faltando");
			requestContext.abortWith(Response.status(400).header(HttpHeaders.CONTENT_TYPE, "application/json").entity(obj.toString()).build());
		}else if(!parametro.equals(Mock.TOKEN)) {
			obj.put("success", false);
			obj.put("message", "Usuário não autorizado");
			requestContext.abortWith(Response.status(401).header(HttpHeaders.CONTENT_TYPE, "application/json").entity(obj.toString()).build());
		
		}
	}

}
