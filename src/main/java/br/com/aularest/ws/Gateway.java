package br.com.aularest.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import be.com.aularest.helper.Helper;
import br.com.aularest.annotations.Secured;
import br.com.aularest.beans.UserBean;
import br.com.aularest.mock.Mock;

@Path("/api")
public class Gateway {
	
	@GET
	@Path("/getinfo")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfo() {
		JSONObject obj = new JSONObject();
		obj.put("success", true);
		obj.put("message", "Test da annotation Secured");
		return Response.status(200).entity(obj.toString()).build();
	}
	
	@GET
	@Secured
	@Path("/getcep")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCep(@HeaderParam("cep")String cep) {
		JSONObject obj = new JSONObject();
		Helper helper = new Helper();
		if(cep==null) {
			obj.put("success", false);
			obj.put("message", "Parametro inexistente");
			return Response.status(400).entity(obj.toString()).build();
		}
		String json = helper.getCepFromDb(cep);
		if(json.equals("error")) {
			obj.put("success", false);
			obj.put("message", "Cep não encontrado");
			return Response.status(404).entity(obj.toString()).build();
		}
		return Response.status(200).entity(json).build();
	}
	
	@POST
	@Path("/auth")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response auth(String json) {
		JSONObject obj = new JSONObject(json);
		JSONObject retObj = null;
		String user = obj.getString("user");
		String pass = obj.getString("password");
		if(user==null && pass==null) {
			retObj = new JSONObject();
			retObj.put("success", false);
			retObj.put("message", "Atributos invalidos");
			return Response.status(400).entity(retObj.toString()).build();
		}
		UserBean bean = Mock.autentica(user, pass);
		if(bean==null) {
			retObj = new JSONObject();
			retObj.put("success", false);
			retObj.put("message", "Autenticação Invalida");
			return Response.status(401).entity(retObj.toString()).build();
		}
		retObj = new JSONObject(bean);
		return Response.status(200).entity(retObj.toString()).build();
	}
	
	@GET
	@Secured
	@Path("twofactor")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getToken(@HeaderParam("tokentwo")String token) {
		Helper helper = new Helper();
		boolean test = helper.checkToken(token);
		JSONObject obj;
		if(test) {
			obj = new JSONObject();
			obj.put("logado", true);
			return Response.status(200).entity(obj.toString()).build();
		}
		obj = new JSONObject();
		obj.put("logado", false);
		return Response.status(401).entity(obj.toString()).build();
		
	}
	
}
