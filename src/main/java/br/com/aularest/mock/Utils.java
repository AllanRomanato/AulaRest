package br.com.aularest.mock;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
	private static String hash(byte[] input) throws NoSuchAlgorithmException{
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        return sb.toString();
	}
	
	public static String generateToken() {
		Long mock = System.currentTimeMillis();
		try {
			String token = hash(String.valueOf(mock).getBytes());
			return token;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
		
	}
	public static void main(String[] args) throws InterruptedException {
		for(int i=0; i<1000; i++) {
			System.out.println(generateToken());
			Thread.sleep(500);
		}
	}
}
