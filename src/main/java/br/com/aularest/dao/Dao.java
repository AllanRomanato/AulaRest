package br.com.aularest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.aularest.beans.CepBean;

public class Dao {
	private Connection conn;
	
	public Dao(Connection conn) {
		this.conn = conn;
	}
	
	public CepBean getInformation(String cep) {
		CepBean cepBean = new CepBean();
		String sql = "SELECT endereco.endereco_logradouro,bairro.bairro_descricao,cidade.cidade_descricao,uf.uf_descricao,uf.uf_sigla " + 
				"FROM endereco,bairro,cidade,uf " + 
				"WHERE endereco.bairro_codigo = bairro.bairro_codigo AND " + 
				"bairro.cidade_codigo = cidade.cidade_codigo AND " + 
				"cidade.uf_codigo = uf.uf_codigo AND " + 
				"endereco_cep = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, cep);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				cepBean.setSuccess(true);
				cepBean.setCep(cep);
				cepBean.setEndereco(rs.getString("endereco_logradouro"));
				cepBean.setBairro(rs.getString("bairro_descricao"));
				cepBean.setCidade(rs.getString("cidade_descricao"));
				cepBean.setEstado(rs.getString("uf_descricao"));
				cepBean.setSigla(rs.getString("uf_sigla"));
			}
			return cepBean;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}
	public void closeConn() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
