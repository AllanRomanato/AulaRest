package be.com.aularest.helper;

import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;

import br.com.aularest.beans.CepBean;
import br.com.aularest.dao.ConnectionFactory;
import br.com.aularest.dao.Dao;
import br.com.aularest.mock.TOTP;

public class Helper {

	public String getCepFromDb(String cep) {
		Dao dao = new Dao(new ConnectionFactory().getConnection());
		CepBean bean = dao.getInformation(cep);
		dao.closeConn();
		if(bean.getCep()==null) {
			return "error";
		}
		JSONObject obj = new JSONObject(bean);
		return obj.toString();
	}
	
	public boolean checkToken(String token) {
		try {
			String tok = TOTP.getToken();
			return token.equals(tok);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}
}
